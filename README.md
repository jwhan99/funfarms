FunFarms
==========

We made an im chat android application base on firebase in this project

# Screenshots
![ChatActivity](https://i.ibb.co/2NwWdHQ/1.jpg)
![MainActivity](https://i.ibb.co/bFQjTrd/1.jpg)
![MessageNotification](https://i.ibb.co/sHKWhWF/1.jpg)

# Usage
Please create a firebase application at first and you will get a google-service.json file, move it to this project and compile the whole project.
BTW DON'T FORGET to add the `REMOTE_MSG_AUTHORIZATION="key=xxx"` in the local.properties file, xxx is your server key in Cloud Messaging API of firebase.