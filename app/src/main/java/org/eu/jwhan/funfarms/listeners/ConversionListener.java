package org.eu.jwhan.funfarms.listeners;

import org.eu.jwhan.funfarms.models.User;

/**
 * @author: jwhan
 * @createTime: 2022/09/04 9:31 PM
 * @description:
 */
public interface ConversionListener {
    void onConversionClicked(User user);
}
