package org.eu.jwhan.funfarms.activities;

import android.os.Bundle;
import android.os.PersistableBundle;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.eu.jwhan.funfarms.util.Constants;
import org.eu.jwhan.funfarms.util.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * @author: jwhan
 * @createTime: 2022/09/04 10:00 PM
 * @description:
 */
public class BaseActivity extends AppCompatActivity {

    private DocumentReference documentReference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager preferenceManager = new PreferenceManager(getApplicationContext());
        FirebaseFirestore database = FirebaseFirestore.getInstance();
        String userId = preferenceManager.getString(Constants.KEY_USER_ID);
        documentReference = database.collection(Constants.KEY_COLLECTION_USERS)
                .document(userId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        documentReference.update(Constants.KEY_ONLINE, 1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        documentReference.update(Constants.KEY_ONLINE, 0);
    }
}
