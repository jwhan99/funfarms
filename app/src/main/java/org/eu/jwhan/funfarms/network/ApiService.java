package org.eu.jwhan.funfarms.network;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

/**
 * @author: jwhan
 * @createTime: 2022/09/04 10:28 PM
 * @description:
 */
public interface ApiService {
    @POST("send")
    Call<String> sendMessage(
            @HeaderMap HashMap<String, String> headers,
            @Body String messagebody
    );
}
