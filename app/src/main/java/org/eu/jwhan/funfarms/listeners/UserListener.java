package org.eu.jwhan.funfarms.listeners;

import org.eu.jwhan.funfarms.models.User;

/**
 * @author: jwhan
 * @createTime: 2022/09/03 8:23 PM
 * @description:
 */
public interface UserListener {
    void onUserClicked(User user);
}
